DROP TABLE IF EXISTS ipsum_quote;

CREATE TABLE ipsum_quote (
  id VARCHAR(32)  PRIMARY KEY,
  title VARCHAR(250) NOT NULL,
  description VARCHAR(1024) NOT NULL,
  should_be_shown boolean NOT NULL,
  date TIMESTAMP NOT NULL,
  sort int auto_increment
);
    