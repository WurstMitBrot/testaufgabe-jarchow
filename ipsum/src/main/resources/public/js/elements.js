$(document).ready(function() {

	$('#upload-button').click(function() {
		$('#json-file-input').click();
	});

	$('#json-file-input').change(function(){
		var targetUrl = $('#json-file-input').data('url');
		var formData = new FormData();
		var file = $('#json-file-input')[0].files[0];
		formData.append("jsonFile", file);
		
		$.ajax({
		method: 'POST',
			url: targetUrl,
			data: formData,
			enctype : 'multipart/form-data',
			contentType : false,
			cache : false,
			processData : false,
			success: function(data){
				// prüfe ob einträge vorhanden sind
				var $elementCard = $('.element-card');
				if($elementCard[0]){
					$elementCard.last().after(data);
				}else {
					$('#element-container').html(data);
					$('#elements-empty').addClass('d-none');
				}
				showSnackbarTextFromElement('#msg-import-success', false);
			},
			error: function(){
				showSnackbarTextFromElement('#msg-import-error', true);
			}
		});
	});

	$('#clear-button').click(function(){
		var targetUrl = $(this).data('url');
		
		$.ajax({
			method: 'GET',
			url: targetUrl,
			success: function(){
				$('#element-container').empty();
				$('#elements-empty').removeClass('d-none');
				showSnackbarTextFromElement('#msg-wipe-success', false);
			},
			error: function(){
				showSnackbarTextFromElement('#msg-wipe-error', true);
			}
		});
	});
	
	$('#element-container').on('click', '.card-remove-button', function(){
		var targetUrl = $(this).data('url');
		var $parent = $(this).closest('.element-card');
		var id = $parent.attr('id');
		
		$.ajax({
			method: 'POST',
			url: targetUrl,
			data: {
				id: id,
			},
			success: function(data){
				$('#' + data.id).remove();
				showSnackbarTextFromElement('#msg-entry-remove-success', false);
				var $elementCard = $('.element-card');
				if(!$elementCard[0]){
					$('#elements-empty').removeClass('d-none');
				}
			},
			error: function(){
				showSnackbarTextFromElement('#msg-entry-remove-error', true);
			}
		});

	});
	
	$('#element-container').on('click', '.move-button', function(){
		var targetUrl = $(this).data('url');
		var $parent = $(this).closest('.element-card');
		var id = $parent.attr('id');
		var isMoveUp = $(this).hasClass('move-button-up');
		
		$.ajax({
			method: 'POST',
			url: targetUrl,
			data: {
				id: id,
			},
			success: function(data){
				if(isMoveUp){
					$('#' + data.id).after($('#' + data.idSwap));						
				}else{
					$('#' + data.id).before($('#' + data.idSwap));
				}
				showSnackbarTextFromElement('#msg-entry-move-success', false);
			},
			error: function(){
				showSnackbarTextFromElement('#msg-entry-move-error', true);
			}
		});
	});
	
	function showSnackbarTextFromElement(elementId, isError){
		var msg = $(elementId).text();
		showSnackbar(msg, isError);
	}
	
	function showSnackbar(msg, isError) {
		var $snackbar = $('#snackbar');
		if(isError){
			$snackbar.addClass('bg-danger');
			$snackbar.removeClass('bg-success');
		}else{
			$snackbar.addClass('bg-success');
			$snackbar.removeClass('bg-danger');
		}
		
		$snackbar.addClass('show');
		$snackbar.html(msg);
		setTimeout(function(){
			$snackbar.removeClass('show');
		}, 3000);
	}

});