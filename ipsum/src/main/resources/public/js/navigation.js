$(document).ready(function() {

	$('.navigation-button').click(function(event) {
		event.preventDefault();
		$('.navigation-button').removeClass('navigation-button-active');
		$(this).addClass('navigation-button-active');
		
		var url = $(this).data('url');

		$.ajax({
			method: 'GET',
			url: url,
			dataType: 'html',
			success: function(data) {
				$('#page-content').html(data);
			},
			error: function(data) {
				$('#page-content').html(data);
			}
		});
	});
});
