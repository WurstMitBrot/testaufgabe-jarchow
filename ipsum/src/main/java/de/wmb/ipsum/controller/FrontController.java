package de.wmb.ipsum.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import de.wmb.ipsum.dto.IpsumQuoteDto;
import de.wmb.ipsum.service.IpsumQuoteService;

@Controller
public class FrontController {
	
	@Autowired
	private IpsumQuoteService ipsumQuoteService;
	
	@GetMapping("/")
	public String showDefault(Model model) {
		
		Set<IpsumQuoteDto> ipsumQuotes = ipsumQuoteService.findAllIpsumQuotes();
		ipsumQuotes.removeIf(ipsumQuote -> !ipsumQuote.getShouldBeShown());
		model.addAttribute("elements", ipsumQuotes);
		return "index";
		
	}
	
}
