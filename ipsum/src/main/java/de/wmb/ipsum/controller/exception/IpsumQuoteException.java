package de.wmb.ipsum.controller.exception;

public class IpsumQuoteException extends Exception{

	private static final long serialVersionUID = -444897279585163681L;

	public IpsumQuoteException(String msg) {
		super(msg);
	}
	
}
