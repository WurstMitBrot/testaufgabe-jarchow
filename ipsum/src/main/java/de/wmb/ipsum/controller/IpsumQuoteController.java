package de.wmb.ipsum.controller;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import de.wmb.ipsum.controller.exception.IpsumQuoteException;
import de.wmb.ipsum.dto.IpsumQuoteDto;
import de.wmb.ipsum.dto.SwapIpsumQuoteDto;
import de.wmb.ipsum.service.IpsumQuoteService;

@Controller
@RequestMapping("/ajax")
public class IpsumQuoteController {

	@Autowired
	private IpsumQuoteService ipsumQuoteService;
	
	@PostMapping(value = "/readJson", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ModelAndView handleFileUpload(ModelAndView modelAndView, @RequestParam("jsonFile") MultipartFile jsonFile ) {
		
		modelAndView.setViewName("fragments/elements :: element-container");
		
		Set<IpsumQuoteDto> newIpsumQuotes = new HashSet<IpsumQuoteDto>();
		try {
			newIpsumQuotes = ipsumQuoteService.importIpsumQuotesFromJson(jsonFile);
			newIpsumQuotes.removeIf(ipsumQuote -> !ipsumQuote.getShouldBeShown());
			modelAndView.addObject("elements", newIpsumQuotes);
		} catch (IpsumQuoteException e) {
			modelAndView.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		
		return modelAndView; 
	}
	
	@GetMapping("/clearData")
	public ResponseEntity<String> clearData() {
		
		ipsumQuoteService.deleteAll();

		return new ResponseEntity<>("All data deleted", HttpStatus.OK);
	}
	
	@PostMapping( value = "/deleteById", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteById(@RequestParam("id") String id ) {
		
		try {
			ipsumQuoteService.deleteById(id);
			JsonObject json = new JsonObject();
			json.addProperty("id", id);
			return new ResponseEntity<>(json.toString(), HttpStatus.OK);
		} catch (IpsumQuoteException e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping( value = "/moveUp", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> moveUp(@RequestParam("id") String id){
			
		Optional<SwapIpsumQuoteDto> swapIpsumQuote = ipsumQuoteService.moveUp(id);

		if(swapIpsumQuote.isPresent()) {
			Gson gson = new Gson();
			String json = gson.toJson(swapIpsumQuote.get());
			return new ResponseEntity<>(json, HttpStatus.OK);
		}else {
			JsonObject json = new JsonObject();
			json.addProperty("id", id);
			return new ResponseEntity<>(json.toString() , HttpStatus.I_AM_A_TEAPOT);
		}
	}
	
	@PostMapping( value = "/moveDown", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> moveDown(@RequestParam("id") String id){
		
		Optional<SwapIpsumQuoteDto> swapIpsumQuote = ipsumQuoteService.moveDown(id);

		if(swapIpsumQuote.isPresent()) {
			Gson gson = new Gson();
			String json = gson.toJson(swapIpsumQuote.get());
			return new ResponseEntity<>(json, HttpStatus.OK);
		}else {
			JsonObject json = new JsonObject();
			json.addProperty("id", id);
			return new ResponseEntity<>(json.toString() , HttpStatus.I_AM_A_TEAPOT);
		}
		
	}
	
}
