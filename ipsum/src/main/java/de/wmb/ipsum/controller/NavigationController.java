package de.wmb.ipsum.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import de.wmb.ipsum.dto.IpsumQuoteDto;
import de.wmb.ipsum.service.IpsumQuoteService;

@Controller
@RequestMapping("/ajax/nav")
public class NavigationController {

	@Autowired
	private IpsumQuoteService ipsumQuoteService;
	
	@GetMapping("/element/list")
	public ModelAndView showElements(ModelAndView modelAndView) {
		Set<IpsumQuoteDto> ipsumQuotes = ipsumQuoteService.findAllIpsumQuotes();
		ipsumQuotes.removeIf(ipsumQuote -> !ipsumQuote.getShouldBeShown());
		
		modelAndView.addObject("elements", ipsumQuotes);
		modelAndView.setStatus(HttpStatus.OK);
		modelAndView.setViewName("fragments/elements :: element-page");
		
		return modelAndView;
	}
	
	@GetMapping("/message")
	public String showMessage(Model model) {
		return "fragments/messages :: message-page";
	}
}
