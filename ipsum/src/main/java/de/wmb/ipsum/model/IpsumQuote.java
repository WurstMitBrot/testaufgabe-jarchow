package de.wmb.ipsum.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "ipsum_quote")
public class IpsumQuote {

	@Id
	@Column
	private String id;
	
	@NotEmpty
	@Column
	private String title;
	
	@NotEmpty
	@Column
	private String description;
	
	@Column(columnDefinition = "boolean default true")
	private Boolean shouldBeShown;
	
	@NotNull
	@Column
	private LocalDateTime date;
	
	@Column
	private Integer sort;

	public IpsumQuote() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getShouldBeShown() {
		return shouldBeShown;
	}

	public void setShouldBeShown(Boolean shouldBeShown) {
		this.shouldBeShown = shouldBeShown;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Override
	public String toString() {
		return "IpsumQuote [id=" + id + ", title=" + title + ", description=" + description + ", shouldBeShown="
				+ shouldBeShown + ", date=" + date + ", sort=" + sort + "]";
	}
	
}
