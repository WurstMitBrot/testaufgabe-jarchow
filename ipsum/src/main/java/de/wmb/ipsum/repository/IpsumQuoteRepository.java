package de.wmb.ipsum.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import de.wmb.ipsum.model.IpsumQuote;

public interface IpsumQuoteRepository extends JpaRepository<IpsumQuote, String> {

	public Set<IpsumQuote> findAllByOrderBySortAsc();
	
	@Query(value = "SELECT ip FROM IpsumQuote ip where ip.sort < :sort ORDER BY ip.sort DESC")
	public List<IpsumQuote> findAllAbove(Integer sort);
	
	@Query(value = "SELECT ip FROM IpsumQuote ip where ip.sort > :sort ORDER BY ip.sort ASC")
	public List<IpsumQuote> findAllBelow(Integer sort);

}
