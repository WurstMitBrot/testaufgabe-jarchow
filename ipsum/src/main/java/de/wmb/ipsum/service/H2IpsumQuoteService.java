package de.wmb.ipsum.service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import de.wmb.ipsum.controller.exception.IpsumQuoteException;
import de.wmb.ipsum.dto.IpsumQuoteDto;
import de.wmb.ipsum.dto.IpsumQuoteRestDto;
import de.wmb.ipsum.dto.SwapIpsumQuoteDto;
import de.wmb.ipsum.model.IpsumQuote;
import de.wmb.ipsum.repository.IpsumQuoteRepository;

@Service
public class H2IpsumQuoteService implements IpsumQuoteService {
	
	static final String DATE_PATTERN = "EE MMM dd yyyy HH:mm:ss 'GMT'Z '('z')'";
	@Autowired
	private IpsumQuoteRepository ipsumQuoteRepository;
	
	@Override
	public Set<IpsumQuoteDto> findAllIpsumQuotes() {
		
		ModelMapper modelMapper = new ModelMapper();
		Set<IpsumQuote> ipsumQuotes = new LinkedHashSet<>();
		ipsumQuotes = ipsumQuoteRepository.findAllByOrderBySortAsc();
		
		Set<IpsumQuoteDto> ipsumQuoteDtos = new LinkedHashSet<>();
		ipsumQuoteDtos = ipsumQuotes.stream().map(ipsumQuote -> modelMapper.map(ipsumQuote, IpsumQuoteDto.class)).collect(Collectors.toCollection( LinkedHashSet::new ));
		
		return ipsumQuoteDtos;
	}

	@Override
	public Set<IpsumQuoteDto> importIpsumQuotesFromJson(MultipartFile jsonFile) throws IpsumQuoteException{
		Set<IpsumQuoteDto> savedIpsumQuoteDtos = new LinkedHashSet<>();
		try {
			ModelMapper modelMapper = new ModelMapper();
			String jsonString = new String(jsonFile.getBytes(), StandardCharsets.UTF_8);

			Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
				@Override
				public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
						throws JsonParseException {
					
					DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();
					builder.parseCaseInsensitive();
					builder.appendPattern(DATE_PATTERN);
					DateTimeFormatter dateFormat = builder.toFormatter(Locale.ENGLISH);
					
					return LocalDateTime.parse(json.getAsString(), dateFormat);
				}
			}).create();
			
			Set<IpsumQuoteRestDto> ipsumQuoteDtos = gson.fromJson(jsonString,new TypeToken<Set<IpsumQuoteRestDto>>(){}.getType());
			Set<IpsumQuote> ipsumQuotes = ipsumQuoteDtos.stream().map(ipsumQuote -> modelMapper.map(ipsumQuote, IpsumQuote.class)).collect(Collectors.toCollection(LinkedHashSet::new));
			
			// Only return elements which aren't already in the database
			for(IpsumQuote ipsumQuote : ipsumQuotes) {
				if(!ipsumQuoteRepository.existsById(ipsumQuote.getId())) {
					IpsumQuote savedIpsumQuote = ipsumQuoteRepository.save(ipsumQuote);
					savedIpsumQuoteDtos.add(modelMapper.map(savedIpsumQuote, IpsumQuoteDto.class));
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			throw new IpsumQuoteException("Error reading multipartfile");
		}
		
		return savedIpsumQuoteDtos;
	}

	@Override
	public void deleteAll() {
		
		ipsumQuoteRepository.deleteAll();
		
	}

	@Override
	public String deleteById(String id) throws IpsumQuoteException{
		
		if(ipsumQuoteRepository.existsById(id)) {
			ipsumQuoteRepository.deleteById(id);
		}else {
			throw new IpsumQuoteException("IpsumQuote with id " + id + " does not exist.");
		}
		
		return id;
	}

	@Override
	public Optional<SwapIpsumQuoteDto>  moveUp(String id) {
		
		return swapSortNumber(id, true);
		
	}

	@Override
	public Optional<SwapIpsumQuoteDto> moveDown(String id) {
		
		return swapSortNumber(id, false);
		
	}
	
	/**
	 * This method gets the IpsumQuote by given id and move it up or down
	 * in the sort order. The direction is defined by the isMoveUp parameter.
	 * 
	 * @param id ID of IpsumQuote to be swaped
	 * @param isMoveUp Direction we want to swap
	 * @return Optional of SwapIspumQuoteDto object as a wrapper object with the ids of the swaped IpsumQuotes
	 */
	private Optional<SwapIpsumQuoteDto> swapSortNumber(String id, boolean isMoveUp) {
		Optional<SwapIpsumQuoteDto> swapIpsumQuoteDtoOpt = Optional.empty();
		Optional<IpsumQuote> ipsumQuote = ipsumQuoteRepository.findById(id);
		
		if(ipsumQuote.isPresent()) {
			List<IpsumQuote> quotesToSwap = new ArrayList<>();
			
			if(isMoveUp) {
				quotesToSwap = ipsumQuoteRepository.findAllAbove(ipsumQuote.get().getSort());
			} else {
				quotesToSwap = ipsumQuoteRepository.findAllBelow(ipsumQuote.get().getSort());
			}
			
			if(!quotesToSwap.isEmpty()) {
				Integer tmpSort = ipsumQuote.get().getSort();

				List<IpsumQuote> swapSortList = new ArrayList<>();
				IpsumQuote ipsumQuoteAbove = quotesToSwap.get(0);
				ipsumQuote.get().setSort(ipsumQuoteAbove.getSort());
				ipsumQuoteAbove.setSort(tmpSort);
				
				swapSortList.add(ipsumQuoteAbove);
				swapSortList.add(ipsumQuote.get());
				
				ipsumQuoteRepository.saveAll(swapSortList);
				SwapIpsumQuoteDto swapIpsumQuoteDto = new SwapIpsumQuoteDto();
				swapIpsumQuoteDto.setId(ipsumQuote.get().getId());
				swapIpsumQuoteDto.setIdSwap(ipsumQuoteAbove.getId());
				swapIpsumQuoteDtoOpt = Optional.of(swapIpsumQuoteDto);
			}
		}
		return swapIpsumQuoteDtoOpt;
	}

}
