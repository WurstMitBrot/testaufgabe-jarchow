package de.wmb.ipsum.service;

import java.util.Optional;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import de.wmb.ipsum.controller.exception.IpsumQuoteException;
import de.wmb.ipsum.dto.IpsumQuoteDto;
import de.wmb.ipsum.dto.SwapIpsumQuoteDto;

public interface IpsumQuoteService {
	
	/**
	 * Finds all IpsumQuotes from database and returns them as a set of DTOs.
	 * @return
	 * A set of IpsumQuoteDto.
	 */
	public Set<IpsumQuoteDto> findAllIpsumQuotes();
	
	/**
	 * Takes a MultipartFile and converts the json data into IpsumQuote Objects.
	 * These objects will be persisted into the database.
	 * 
	 * @param jsonFile MultipartFile with json data
	 * @return Returns a set of newly written IpsumQuotes
	 * @throws IpsumQuoteException
	 */
	public Set<IpsumQuoteDto> importIpsumQuotesFromJson(MultipartFile jsonFile) throws IpsumQuoteException;
	
	/**
	 * Deletes a IspumQuote with the given id.
	 * @param id Id of theIpsumQuote to be deleted
	 * @return Returns the id of the deleted object. If deletion successful.
	 * @throws IpsumQuoteException
	 */
	public String deleteById(String id) throws IpsumQuoteException;

	/**
	 * Deletes every IpsumQuote from the database.
	 */
	public void deleteAll();
	
	/**
	 * Swaps the sort number to the element with the next lower number.
	 * The element will be pushed up in the order.
	 * @param id Id of the IpsumQuote to be pushed up
	 * @return Returns a wrapper class with the ids of the swapped elements.
	 */
	public Optional<SwapIpsumQuoteDto> moveUp(String id);
	
	/**
	 * Swaps the sort number to the element with the next higher number.
	 * The element will be pushed down in the order.
	 * @param id Id of the IpsumQuote to be pushed down
	 * @return A wrapper class with the ids of the swapped elements.
	 */
	public Optional<SwapIpsumQuoteDto> moveDown(String id);
}
