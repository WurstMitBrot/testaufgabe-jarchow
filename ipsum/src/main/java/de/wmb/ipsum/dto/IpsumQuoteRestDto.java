package de.wmb.ipsum.dto;

import java.time.LocalDateTime;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpsumQuoteRestDto {

	@SerializedName("_id")
	@Expose
	private String id;
	@SerializedName("title")
	@Expose
	private String title;
	@SerializedName("descr")
	@Expose
	private String description;
	@SerializedName("shouldBeShown")
	@Expose
	private Boolean shouldBeShown;
	@SerializedName("date")
	@Expose
	private LocalDateTime date;
	
	public IpsumQuoteRestDto() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getShouldBeShown() {
		return shouldBeShown;
	}

	public void setShouldBeShown(Boolean shouldBeShown) {
		this.shouldBeShown = shouldBeShown;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "IpsumQuoteRestDto [id=" + id + ", title=" + title + ", description=" + description + ", shouldBeShown="
				+ shouldBeShown + ", date=" + date + "]";
	}
	
}
