package de.wmb.ipsum.dto;

import java.time.LocalDateTime;

public class IpsumQuoteDto {

	private String id;
	private String title;
	private String description;
	private Boolean shouldBeShown;
	private LocalDateTime date;
	private int sort;
	
	public IpsumQuoteDto() {
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getShouldBeShown() {
		return shouldBeShown;
	}
	public void setShouldBeShown(Boolean shouldBeShown) {
		this.shouldBeShown = shouldBeShown;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	
	
	
}
