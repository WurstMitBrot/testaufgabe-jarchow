package de.wmb.ipsum.dto;

public class SwapIpsumQuoteDto {
	
	private String id;
	private String idSwap;
	
	public SwapIpsumQuoteDto() {
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdSwap() {
		return idSwap;
	}
	public void setIdSwap(String idSwap) {
		this.idSwap = idSwap;
	}
	
}
