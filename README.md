# Stephan's Job Application Task #

This test task can show your favorite Lorem Ipsum Quotes.

### What is this repository for? ###
This repository is for my job application as a Fullstack Developer at CGM.

### How do I get set up? ###
The repository provides a .jar and a .war file to start the project.

Starting the jar file with command line:
java -jar "PATH/TO/FILE/"ipsum-app.jar de.wmb.ipsum.IpsumApplication

### How to use the project ###
The repository provides 4 json files, with different types of lorem ipsum styles, which can be imported.

### URL ###
localhost:8080/ipsumQuotes
